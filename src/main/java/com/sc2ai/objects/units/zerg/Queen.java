package com.sc2ai.objects.units.zerg;

import org.sikuli.script.Button;
import org.sikuli.script.Location;
import org.sikuli.script.Match;
import org.sikuli.script.Region;
import org.springframework.stereotype.Component;

import com.sc2ai.objects.units.Units;

import lombok.Data;
import lombok.SneakyThrows;

@Data
@Component
public class Queen extends Units{

    @SneakyThrows
    public void build(Location base, Boolean inject) {
        
        actions.mouseClick(base,Button.LEFT);
        s.wait(hatch.getImage(1), org.sikuli.script.Constants.FOREVER);
        
        actions.mouseClick(s.getCenter(),Button.LEFT);
        build(1);
        //addToHotKey after queen is born
//        jobQueue.addDelayTask(this.getClass(), "addToHotKey", 51, queen,base,Constants.QUEENS_HOTKEY);
        
        if(inject)
            jobQueue.addRepeatDelayTask(this.getClass(), "queenInject", 52, 31, base);
    }

    private void build(int i) {
        Region panel = screen.getUnitPanelRegion();
        do
            s.type("q");
        while(panel.exists(getImage(1))==null);
    }
    
    public void queenInject(Location base){
        
        actions.mouseClick(base,Button.LEFT);
        actions.selectUnitsOnScreen();
        Match queenMatch = screen.getUnitPanelRegion().exists(getImage(0));
        
        //Queen not found - build another one
        if(queenMatch==null){
            build(base,false);
            return;
        }
  
        actions.mouseClick(queenMatch.getCenter(),Button.LEFT);
        s.type("x");
        actions.mouseClick(s.getCenter(),Button.LEFT);
    }

}
