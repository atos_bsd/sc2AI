package com.sc2ai.config;

import org.sikuli.script.Region;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.sc2ai.buildorders.BuildOrder;
import com.sc2ai.buildorders.TwoBaseRoachAllin;
import com.sc2ai.maps.Map;
import com.sc2ai.maps.Ulrena;
import com.sc2ai.utils.SikuliScreen;

@Configuration
public class Config {
    
    @Bean 
    public SikuliScreen sikuliScreen(){
       Region screenRegion = new Region(5, 5 ,1895,730);
       Region miniMapRegion = new Region(25, 810,265,256);
       Region unitPanel = new Region(365, 880,990,185);
       Region actionPanel = new Region(1530, 840,370,220);
       
       return new SikuliScreen(screenRegion,miniMapRegion,unitPanel,actionPanel);
    }
       
    @Bean 
    public BuildOrder buildOrder(){
       return new TwoBaseRoachAllin();
    }

    @Bean 
    public Map map(){
       return new Ulrena();
    }
}
