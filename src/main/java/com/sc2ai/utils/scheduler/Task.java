package com.sc2ai.utils.scheduler;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerContext;
import org.springframework.context.ApplicationContext;

import com.sc2ai.textures.Texture;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@Data
@NoArgsConstructor
public class Task implements Job{
    
    private String methodName;
    private Class<?> cls;
    private Object[] argsArray;
    private Class<?>[] clsArray;
    
    public Task(Class<?> classType, String method, Object... args){
        this.cls=classType;
        this.methodName=method;
        this.argsArray=args;
        this.clsArray = new Class<?>[argsArray.length];
        
        for(int i=0;i<argsArray.length;i++)
            clsArray[i]=(argsArray[i].getClass().getSuperclass().equals(Texture.class)) ? Texture.class :
                argsArray[i].getClass();

    }
    
    @SneakyThrows
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        //Add task that was delayed
        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        String taskName = dataMap.getString("task");
        SchedulerContext schedulerContext = context.getScheduler().getContext();
        Task task = (Task) schedulerContext.get(taskName);
        ApplicationContext contex = (ApplicationContext) schedulerContext.get("contex");
        JobQueue jobQueue = contex.getBean(JobQueue.class);
     
        jobQueue.addTaskFirst(task);
        
        schedulerContext.remove(taskName);
    }
}
