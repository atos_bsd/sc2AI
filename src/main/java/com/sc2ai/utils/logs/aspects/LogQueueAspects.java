package com.sc2ai.utils.logs.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sc2ai.utils.logs.Log;
import com.sc2ai.utils.scheduler.Task;

@Aspect
@Component
public class LogQueueAspects {
    
    @Autowired
    @Qualifier("logsQueueImpl")
    private Log logQueue;
    
    @Before("execution(* com.sc2ai.utils.scheduler.JobQueue.addTask(..))")
    public void addTask(JoinPoint jp){;
        String className =  ((Class) jp.getArgs()[0]).getSimpleName();
        String methodName = (String) jp.getArgs()[1];

        logQueue.addQueneMessage(className+" "+methodName);
    }

    @Before("execution(* com.sc2ai.utils.scheduler.JobQueue.addTaskFirst(..))")
    public void addFirstTask(JoinPoint jp){
        Task task = (Task) jp.getArgs()[0];
        logQueue.addFirstQueneMessage(task.getCls().getSimpleName()+" "+task.getMethodName());
    }

    @Before("execution(* com.sc2ai.objects..*.*(..))")
    public void executeTask(JoinPoint jp){
      logQueue.executeQueneMessage(getTaskName(jp));
    }

    @After("execution(* com.sc2ai.objects..*.*(..))")
    public void removeTask(JoinPoint jp){
      logQueue.removeQueneMessage(getTaskName(jp));
    }
    
    private String getTaskName(JoinPoint jp) {     
        return jp.getSignature().getDeclaringType().getSimpleName()+" "+jp.getSignature().getName();
    }
}
