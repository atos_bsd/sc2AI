package com.sc2ai.objects.buildings;

import org.sikuli.script.Button;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.springframework.beans.factory.annotation.Autowired;

import com.sc2ai.buildorders.BuildOrder;
import com.sc2ai.maps.Map;
import com.sc2ai.objects.Objects;
import com.sc2ai.objects.units.zerg.Drone;
import com.sc2ai.textures.Texture;
import com.sc2ai.utils.scheduler.JobQueue;

import lombok.SneakyThrows;

public class Building extends Objects{
    
    @Autowired
    protected Map map;
    
    @Autowired
    protected Drone drone;
    
    @Autowired
    protected BuildOrder buildOrder;
    
    @SneakyThrows
    protected void buildBuilding(String typeOfBuilding, Texture texture) {
        Region actionRegion= screen.getActionPanelRegion();
        Pattern image = (Pattern) texture.getImageList().get(0);
        
        s.type(typeOfBuilding);
        
        Match building = actionRegion.find(image);
        //Click while image exist
        do
            actions.mouseClick(building.getCenter(),Button.LEFT);
        while(actionRegion.exists(textyresBean.getEsc().getImageList().get(1))==null);
    }

}
