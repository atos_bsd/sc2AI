package com.sc2ai.maps;

import java.util.LinkedList;

import org.sikuli.script.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sc2ai.utils.SikuliScreen;

import lombok.Data;

@Data
@Component
public class Ulrena implements Map{
   
    @Autowired
    private SikuliScreen screen;
   
    private Location collectionPoint, secondBase, spawningPool;
    
	private LinkedList<Location> basesList = new LinkedList<Location>();
    private LinkedList<Location> extractorList = new LinkedList<Location>();
    private Integer enemyYDiferent;
    
    public Ulrena(){
        
    	collectionPoint = new Location(110,893);
    	secondBase = new Location(49,9);
    	enemyYDiferent = 135;
    	spawningPool = new Location(-20,-250);

    	
    	extractorList.add(new Location(850,150));
    }
    
    public Location getCollectionPoint() {
    	
    	if(screen.getEnemyUp())
    		collectionPoint.setLocation(collectionPoint.getX(), collectionPoint.getY()+92);
    	
		return collectionPoint;
	}

	@Override
    public Location getFirstOverCoor(Location enemyPoint) {
        return new Location(screen.getEnemyPoint().getX()+25, screen.getEnemyPoint().getY());
    }

    @Override
    public void setMainBaseToMapList() {
        Location firstBase = new Location(screen.getMyPoint().getX(),screen.getMyPoint().getY());
        
        if(screen.getEnemyUp())
        	secondBase.setLocation(secondBase.getX(), secondBase.getY()*-1.4);
        
        basesList.add(firstBase);
        basesList.add(new Location(firstBase.getX()+secondBase.getX(), firstBase.getY()+secondBase.getY()));
    }

    public Location getSpawningPool() {
    	    	
    	Location base = screen.getS().getCenter();
    	Location pool = new Location(base.getX()+spawningPool.getX(), base.getY()+spawningPool.getY());
    	
		return pool;
	}

}
