package com.sc2ai.maps;

import java.util.List;

import org.sikuli.script.Location;

public interface Map {
    
    Location getFirstOverCoor(Location enemyPoint);
    List<Location> getBasesList();
    List<Location> getExtractorList();
    
    void setMainBaseToMapList();
	Location getCollectionPoint();
	Location getSpawningPool();
	Integer getEnemyYDiferent();
    
}
