package com.sc2ai.config;

import org.sikuli.script.Key;

public final class Constants {

	public static final String RESOURCES_IMAGES = "./resources/images/";
	public static final Double DEFAULT_PATTERN_SIMILARITY = 0.8;
	
	
	public static final String MY_POINT = "/greenPoint.jpg";
	public static final String ENEMY_POINT = "/Enemy_Point.jpg";
	
	public static final String FIRST_BASE_HOTKEY = Key.F1;
	public static final String SECOND_BASE_HOTKEY = Key.F2;

	
	public static final String BASES_HOTKEY = "4";
	public static final String QUEENS_HOTKEY = "5";
	public static final String ARMY_HOTKEY = "1";
	
	
	public static final int EXTRACTOR_BUILD_TIME = 18;
	public static final int HATCH_BUILD_TIME = 55;


    
}
