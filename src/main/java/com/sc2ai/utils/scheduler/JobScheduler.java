package com.sc2ai.utils.scheduler;

import java.util.Date;

import javax.annotation.PostConstruct;
import org.quartz.JobBuilder;

import org.quartz.JobDetail;
import org.quartz.ScheduleBuilder;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import lombok.SneakyThrows;

@Component
public class JobScheduler {
    
    @Autowired
    private ApplicationContext contex;
    
    private Scheduler scheduler;
    
    @SneakyThrows
    public JobScheduler(){
    	scheduler = new StdSchedulerFactory().getScheduler();
    	scheduler.start();
    }
    
    @SneakyThrows
    @PostConstruct
    public void init(){
        scheduler.getContext().put("contex", contex);
    }
    
    
    @SneakyThrows
    public void addJob(Task task, Integer delayTime, Integer repeat){
    	Trigger trigger;
        String taskName = task.getMethodName()+new Date(new Date().getTime() + delayTime*1000);
        
        JobDetail job = JobBuilder.newJob(Task.class)
      	      .withIdentity("Job", taskName)
      	      .usingJobData("task", taskName)
      	      .build();

        trigger = TriggerBuilder.newTrigger()
      		.withIdentity("Trigger", taskName)
      		.startAt(new Date(new Date().getTime() + delayTime*1000))
      		.build();
        
        if(repeat!=null)
        	trigger = TriggerBuilder.newTrigger()
	      		.withIdentity("Trigger", taskName)
	      		.startAt(new Date(new Date().getTime() + delayTime*1000))
	      		.withSchedule(SimpleScheduleBuilder.simpleSchedule()
	      	          .withIntervalInSeconds(repeat)
	      	          .repeatForever())
	      		.build();
      
        scheduler.getContext().put(taskName, task);
        scheduler.scheduleJob(job, trigger);
    }
}
