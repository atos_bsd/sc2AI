package com.sc2ai.utils.logs.frame;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.awt.geom.QuadCurve2D;
import java.util.LinkedList;

public class LogsFrame extends JFrame {
	
	private static final String EXECUTE_CURSOR = "--> ";
    private LinkedList<Label> messageList = new LinkedList<Label>();
	
    public LogsFrame(int x, int y) {
        super("ShapedWindow");
 //       setLayout(new GridBagLayout());
        setSize(300, 200);
       // setBounds(, 80, 300, 200);

        // It is best practice to set the window's shape in
        // the componentResized method.  Then, if the window
        // changes size, the shape will be correctly recalculated.
        addComponentListener(new ComponentAdapter() {
            // Give the window an elliptical shape.
            // If the window is resized, the shape is recalculated here.
            @Override
            public void componentResized(ComponentEvent e) {
                setShape(new QuadCurve2D.Double(0,0,getWidth(),getHeight(),0,0));
            }
        });

        setUndecorated(true);
        setAlwaysOnTop(true);
        setLocationByPlatform( true );
        setLocation(x, y);
        setSize(getWidth(),getHeight());
 //       setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        setLayout(null);
    }

    public void addMessage(String message, Color color, Boolean deleteMessage) {  	
        Label label = new Label(message);
        
        if(color!=null)
            label.setForeground(color);
        
        if(!messageList.isEmpty())
        	moveOtherLabels(deleteMessage);
        
        label.setBounds(0, getHeight()-15, 300, 15);
        
        add(label);
        messageList.addFirst(label);
        System.out.println(message);
	}

	private void moveOtherLabels(Boolean deleteMessages) {
		moveLabels(0,true);
        
		if(deleteMessages)
		    removeNonVisibleMessages();
	}

    private void moveLabels(Integer numLabels,Boolean up) {
        Integer movePixel = (up)? -15:15;
        
        messageList.stream()
            .filter(l -> messageList.indexOf(l)>=numLabels)
        	.forEach(l -> l.setBounds(0, l.getY()+movePixel, 300, 15));
    }

    private void removeNonVisibleMessages() {
        if(messageList.size()>12){
        	remove(messageList.getLast());
        	messageList.removeLast();
        }
    }

    public void removeMessage(String message) {        
        Label label = getLabel(message);
        
        if(label==null)
            return;
            
        Integer index = messageList.indexOf(label);
        
        messageList.remove(label);
        remove(label);
        moveLabels(index, false);
    }

    private Label getLabel(String message) {
        Label label = (Label) messageList.stream()
            .filter(l -> l.getText().endsWith(message))
            .findFirst()
            .orElse(null);
        return label;
    }

    public void setExecuteQueneMessage(String message) {
        Label label = getLabel(message);
        
        if(label!=null){
            label.setText(EXECUTE_CURSOR+message);
            label.setForeground(Color.BLUE);
        }
    }

    public void addQueneMessage(String message, Object object) {
        Label label = new Label(message);
       
        if(messageList.isEmpty())
            label.setBounds(0, getHeight()-15, 300, 15);
        else
            label.setBounds(0, messageList.getLast().getY()-15, 300, 15);
        
        add(label);
        messageList.addLast(label);
    }    
}