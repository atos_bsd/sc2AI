package com.sc2ai.actions;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.sikuli.script.Button;
import org.sikuli.script.Env;
import org.sikuli.script.Key;
import org.sikuli.script.Location;
import org.sikuli.script.Match;
import org.sikuli.script.Mouse;
import org.sikuli.script.Screen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sc2ai.config.Constants;
import com.sc2ai.maps.Map;
import com.sc2ai.objects.buildings.zerg.Hatch;
import com.sc2ai.objects.units.zerg.Drone;
import com.sc2ai.objects.units.zerg.Overlord;
import com.sc2ai.textures.Crystal;
import com.sc2ai.textures.Texture;
import com.sc2ai.utils.SikuliScreen;
import com.sc2ai.utils.logs.Log;
import com.sc2ai.utils.scheduler.JobQueue;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@Data
@NoArgsConstructor
@Component
public class Actions {

    @Autowired
    protected SikuliScreen screen;

    @Autowired
    @Qualifier("logsMessageImpl")
    protected Log log;

    @Autowired
    protected Map map;

    @Autowired
    protected JobQueue jobQueue;
    
    @Autowired
    protected Overlord overlord;
 
    @Autowired
    protected Drone drone;
    
    @Autowired
    protected Hatch hatch;

    protected Screen s;
    protected final static Object obj = new Object();
    private Texture crystal;

    @PostConstruct
    public void init() {
        s = screen.getS();
        crystal = new Crystal();
        
        crystal.setSimilarity(0.65);
    }

   @SneakyThrows
	public void setMineralPoint() {
        log.addMessage("Selecting minerals point");
        Match crystalMatch = s.findBest(crystal.getImageList().get(0));
        mouseClick(crystalMatch.getCenter(), Button.RIGHT);
    }

    public void selectUnitsOnScreen() {
        log.addMessage("Selecting units on screen");
        Mouse.move(screen.getScreenRegion().getTopLeft());
        Mouse.down(Button.LEFT);
        Mouse.move(screen.getScreenRegion().getBottomRight());
        Mouse.up();
    }
    
    public void mouseClick(Location location ,int button) {
        Mouse.move(location);
        Mouse.down(button);
        Mouse.up();
    }

    public void setCollectionPoint() {
        log.addMessage("Set collection point");
        mouseClick(map.getCollectionPoint(),Button.LEFT);
        mouseClick(s.getCenter(),Button.RIGHT);
    }

    @SneakyThrows
    public void addToHotKey(Texture unit, Location location, String hotkey) {
        mouseClick(location,Button.LEFT);
        selectUnitsOnScreen();

        Match unitMatch = s.findBest(unit.getImageList().toArray());

        mouseClick(unitMatch.getCenter(),Button.LEFT);
        s.type(hotkey, Key.SHIFT);
    }

    @SneakyThrows
    public void waitTime(int i) {
        synchronized (obj) {
            TimeUnit.SECONDS.sleep(i);
        }
    }
}
