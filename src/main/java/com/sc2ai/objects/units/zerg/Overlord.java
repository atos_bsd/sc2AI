package com.sc2ai.objects.units.zerg;

import org.sikuli.script.Button;
import org.sikuli.script.Location;
import org.sikuli.script.Match;
import org.springframework.stereotype.Component;

import com.sc2ai.objects.units.Units;
import com.sc2ai.textures.Texture;

import lombok.Data;
import lombok.SneakyThrows;

@Data
@Component
public class Overlord extends Units{
    
    
    public void build(Integer count) {
        log.addMessage("=== Start build Over");
        buildUnit("v",true);
    }
    
    @SneakyThrows
    public void move(Location fromScreen, Location toScreen){
        
        actions.mouseClick(fromScreen,Button.LEFT);
        actions.selectUnitsOnScreen();
        log.addMessage("Looking for overlord");
        Match over = screen.getUnitPanelRegion().find(getImage(0));
        
        if(over==null){
            log.addMessage("!!! Error Can't find overlord");
            return;
        }
        
        log.addMessage("Overlord finded");
        log.addMessage("Sent overlord to enemy side");
        over.click();

        actions.mouseClick(toScreen,Button.RIGHT);
    }

}
