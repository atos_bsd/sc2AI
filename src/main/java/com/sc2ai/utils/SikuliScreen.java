package com.sc2ai.utils;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;

import javax.annotation.PostConstruct;

import org.sikuli.basics.Debug;
import org.sikuli.basics.Settings;
import org.sikuli.script.IRobot;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Location;
import org.sikuli.script.Match;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.sc2ai.config.Constants;
import com.sc2ai.maps.Map;
import com.sc2ai.utils.logs.Log;
import com.sc2ai.utils.logs.frame.LogsFrame;
import com.sc2ai.utils.logs.frame.LogsController;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@Component
@Data
@RequiredArgsConstructor
public class SikuliScreen {

    @Autowired
    @Qualifier("logsMessageImpl")
    protected Log log;
	
    @Autowired
    private Map map;
    
    private Screen s;
    private Location enemyPoint, myPoint;
    private Boolean enemyUp;
    
    @NonNull
    private Region screenRegion, miniMapRegion, unitPanelRegion, actionPanelRegion;
    
    @SneakyThrows
    public void findMiniMapPointLocations(){
        
        Match enemyPointMatch = miniMapRegion.exists(Constants.ENEMY_POINT);
        Match myPointMatch = miniMapRegion.find(Constants.MY_POINT);

        myPoint = myPointMatch.getCenter();
        enemyPoint = (enemyPointMatch!=null) ? enemyPointMatch.getCenter():setupEnemyPoint();               
        enemyUp = (enemyPoint.getY()<myPoint.getY()) ? true:false;
        
        map.setMainBaseToMapList();
    }

    @PostConstruct
    public void init() {
    	Debug.on(4);
    	Settings.MinSimilarity=Constants.DEFAULT_PATTERN_SIMILARITY;
        s = new Screen();
        s.setAutoWaitTimeout(0);
        s.setThrowException(false);
        Settings.MoveMouseDelay = 0;
        
        ImagePath.add(Constants.RESOURCES_IMAGES);
        
        screenRegion.setAutoWaitTimeout(1);
        miniMapRegion.setAutoWaitTimeout(1);
        unitPanelRegion.setAutoWaitTimeout(1);
        actionPanelRegion.setAutoWaitTimeout(1);
        
        logPrint();
    }

    private void logPrint() {
        log.addMessage("Screen created");
        
        log.addMessage("screenRegion Regiondinates:"+screenRegion.getX()+","+ screenRegion.getY() +","+screenRegion.getW()+","+screenRegion.getH());
        log.addMessage("miniMapRegion coordinates:"+miniMapRegion.getX()+","+ miniMapRegion.getY() +","+miniMapRegion.getW()+","+miniMapRegion.getH());
        log.addMessage("unitPanelCoor Regiondinates:"+unitPanelRegion.getX()+","+ unitPanelRegion.getY() +","+unitPanelRegion.getW()+","+unitPanelRegion.getH());
        log.addMessage("actionPanelCoor coordinates:"+actionPanelRegion.getX()+","+ actionPanelRegion.getY() +","+actionPanelRegion.getW()+","+actionPanelRegion.getH());
    }
    
    private Location setupEnemyPoint() {
    	Integer y = map.getEnemyYDiferent();
    	
        //if MyPoint>900 enemy on top 
        if(myPoint.getY()>900)
        	y*=-1;
        
        return new Location(myPoint.getX(),myPoint.getY()+y);
    }   
    
}
