package com.sc2ai.objects.buildings.zerg;

import org.sikuli.script.Button;
import org.springframework.stereotype.Component;

import com.sc2ai.objects.buildings.Building;

import lombok.Data;

@Data
@Component
public class SpawningPool extends Building {

    public void build(){
        log.addEmptyMessage();
        log.addMessage("==+ Start build Spawning Pool");
        
        drone.select(map.getBasesList().get(0));
        buildBuilding("z",texture);
        actions.mouseClick(map.getSpawningPool(),Button.LEFT);
    }
}
