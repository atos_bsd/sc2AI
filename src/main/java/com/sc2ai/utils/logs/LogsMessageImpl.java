package com.sc2ai.utils.logs;

import org.springframework.stereotype.Component;

import com.sc2ai.utils.logs.frame.LogsController;

@Component
public class LogsMessageImpl extends LogsController implements Log{

    public LogsMessageImpl(){
        super(1600, 80);
    }
}
