package com.sc2ai.objects.buildings.zerg;

import org.sikuli.script.Button;
import org.sikuli.script.Key;
import org.sikuli.script.Location;
import org.springframework.stereotype.Component;

import com.sc2ai.config.Constants;
import com.sc2ai.objects.buildings.Building;

import lombok.Data;
import lombok.SneakyThrows;

@Data
@Component
public class Hatch extends Building{
   
    public void build(Location location) {
        log.addEmptyMessage();
        log.addMessage("==+ Start build second Base");
        
        drone.select(map.getBasesList().get(0));
        actions.mouseClick(location,Button.LEFT);
        buildBuilding("z",texture);
        actions.mouseClick(s.getCenter(),Button.LEFT);
        
        jobQueue.addDelayTask(this.getClass(), "setBaseToHotKey", Constants.HATCH_BUILD_TIME, Constants.SECOND_BASE_HOTKEY ,map.getBasesList().get(1), true);
    }
    
    @SneakyThrows
    public void waiting() {
        log.addMessage("BuildOrder "+buildOrder.getClass().getSimpleName()+" started");
        log.addMessage("Map "+map.getClass().getSimpleName()); 
        log.addEmptyMessage();
        
        s.wait(getImage(1),org.sikuli.script.Constants.FOREVER);
        
        screen.findMiniMapPointLocations();
        setBaseToHotKey(Constants.FIRST_BASE_HOTKEY,map.getBasesList().get(0),false);
        
        log.addMessage("Hatch finded and added to List");
        buildOrder.start();
    }
    
    public void setBaseToHotKey(String hotkey, Location base, Boolean allBase) {
    	actions.mouseClick(base,Button.LEFT);
        log.addMessage("Set Base hotkey " + hotkey.getClass().getSimpleName());
        s.type(hotkey, Key.CTRL);
        s.getCenter().click();
        s.type(Constants.BASES_HOTKEY, Key.SHIFT);
        actions.setCollectionPoint();
        s.type(hotkey);
        
        if(allBase)
            s.type(Constants.BASES_HOTKEY);        
        
        actions.setMineralPoint();
    }
}
