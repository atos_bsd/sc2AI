package com.sc2ai.objects.units.zerg;

import org.sikuli.script.Key;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.springframework.stereotype.Component;

import com.sc2ai.config.Constants;
import com.sc2ai.objects.units.Units;

import lombok.SneakyThrows;

@Component
public class Ling extends Units{

	@SneakyThrows
    public void build(Integer count, Boolean addToHotKey) {
        log.addMessage("=== Start build "+count*2+" lings");
        
        buildUnit("z", false, count);
        
        if(addToHotKey){
        	Pattern pattern = (Pattern) textyresBean.getEgg().getImageList().stream()
                    .filter(img -> screen.getUnitPanelRegion().exists(img)!=null)
                    .findFirst()
                    .orElse(null);
        	
        	Match match = screen.getUnitPanelRegion().find(pattern);
            
            s.keyDown(Key.CTRL);
            match.click();
            s.keyUp(Key.CTRL);
            s.type(Constants.ARMY_HOTKEY, Key.SHIFT);
        }
          
    }    
}
