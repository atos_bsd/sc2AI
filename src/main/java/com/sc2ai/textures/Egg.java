package com.sc2ai.textures;

import org.sikuli.script.Region;
import org.springframework.stereotype.Component;

import com.sc2ai.objects.units.Units;

import lombok.Data;

@Data
@Component
public class Egg extends Texture{
    
    public boolean isEggPresent(Region region) {
        Object match = getImageList().stream()
                    .filter(img -> region.exists(img)!=null)
                    .findFirst()
                    .orElse(null);

        return match!=null;
    }

}
