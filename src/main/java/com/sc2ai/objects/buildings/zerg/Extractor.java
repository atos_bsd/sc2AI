package com.sc2ai.objects.buildings.zerg;

import org.sikuli.script.Button;
import org.sikuli.script.Location;
import org.springframework.stereotype.Component;

import com.sc2ai.config.Constants;
import com.sc2ai.objects.buildings.Building;
import com.sc2ai.objects.units.zerg.Drone;

import lombok.Data;

@Data
@Component
public class Extractor extends Building {

    public void build(Location base, Location extractorLocation) {
        log.addEmptyMessage();
        log.addMessage("==+ Start build Extractor");

        drone.select(base);
        buildBuilding("z",texture);
        actions.mouseClick(extractorLocation,Button.LEFT);
        jobQueue.addDelayTask(Drone.class, "sentDrone", Constants.EXTRACTOR_BUILD_TIME, base, extractorLocation);       
    }
}
