package com.sc2ai.buildorders;

import org.sikuli.script.Location;
import org.springframework.beans.factory.annotation.Autowired;
import com.sc2ai.maps.Map;
import com.sc2ai.objects.buildings.zerg.Extractor;
import com.sc2ai.objects.buildings.zerg.Hatch;
import com.sc2ai.objects.buildings.zerg.SpawningPool;
import com.sc2ai.objects.units.zerg.Drone;
import com.sc2ai.objects.units.zerg.Ling;
import com.sc2ai.objects.units.zerg.Overlord;
import com.sc2ai.objects.units.zerg.Queen;
import com.sc2ai.utils.SikuliScreen;
import com.sc2ai.utils.scheduler.JobQueue;


public class TwoBaseRoachAllin implements BuildOrder {
    
    @Autowired
    private Map map;
    
    @Autowired
    private SikuliScreen screen;
    
    @Autowired
    private JobQueue jobQueue;

    public void start() {
       
        jobQueue.addTask(Drone.class, "build", 1);
        
        jobQueue.addTask(Overlord.class, "move",new Location(screen.getMyPoint()), map.getFirstOverCoor(screen.getEnemyPoint()));
        jobQueue.addTask(Overlord.class, "build", 1);
        
        jobQueue.addTask(Drone.class, "build", 4);
        
        jobQueue.addTask(Hatch.class, "build", map.getBasesList().get(1));
        jobQueue.addTask(Drone.class, "build", 1);
               
        jobQueue.addTask(Extractor.class, "build", map.getBasesList().get(0),map.getExtractorList().get(0));
        jobQueue.addTask(SpawningPool.class, "build");
        
        jobQueue.addTask(Drone.class, "build", 3);
        jobQueue.addTask(Overlord.class, "build", 1);
        jobQueue.addTask(Drone.class, "build", 1);
        
        jobQueue.addTask(Queen.class, "build", map.getBasesList().get(0), true);
        
        jobQueue.addTask(Ling.class, "build", 2, true);
        
        jobQueue.addTask(Queen.class, "build", map.getBasesList().get(1), true);
    }    

}
