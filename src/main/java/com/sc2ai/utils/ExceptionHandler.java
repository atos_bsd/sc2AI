package com.sc2ai.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.sc2ai.utils.logs.Log;
import com.sc2ai.utils.logs.frame.LogsController;
import com.sc2ai.utils.scheduler.JobQueue;
import com.sc2ai.utils.scheduler.Task;

@MessageEndpoint
@Component
public class ExceptionHandler {
    
    @Autowired
    @Qualifier("logsMessageImpl")
    protected Log log;
    
    @Autowired
    private JobQueue jobQueue;

    @ServiceActivator(inputChannel = "errorChannel")
    public void exceptionHandler(Message<Exception> message){
        
       Task task = jobQueue.getTaskList().getFirst();
    
       log.addErrorMessage("Error in " + task.getCls().getSimpleName()+"->"+task.getMethodName());
       log.addErrorMessage("Task "+task.getMethodName()+" was removed");
       
       jobQueue.getTaskList().remove(task);
       
       System.out.println("ERROR - "+ message.getPayload());
    }
}
