package com.sc2ai.textures;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.sikuli.script.Pattern;

import com.sc2ai.config.Constants;
import com.sc2ai.objects.units.Units;

import lombok.Data;


@Data
public class Texture {
	
	private LinkedList imageList;
	private Double defaultSimilarity;

	public Texture(){
		setImageList(this.getClass());
	}

    public Texture(Object objTexture) {
        setImageList(objTexture.getClass());
    }

    private void setImageList(Class<?> textureClass) {
        imageList = new LinkedList<Pattern>();
		List packageName = new LinkedList(Arrays.asList(textureClass.getPackage().getName().split("\\.")));
		String textureName = textureClass.getSimpleName();
		String race = (String) packageName.get(packageName.size()-1);
		String type = (String) packageName.get(packageName.size()-2);
		
		File folder = new File(Constants.RESOURCES_IMAGES+type+"/"+race+"/"+textureName);
		
		Arrays.asList(folder.listFiles()).stream()
			.forEach(i -> imageList.add(getPattern(i)));
    }

    private Pattern getPattern(File file) {
        Pattern pattern =  new Pattern(file.getAbsolutePath());
        Double similarity = (defaultSimilarity!=null)? defaultSimilarity :Constants.DEFAULT_PATTERN_SIMILARITY;

        pattern.similar(similarity.floatValue());
        
        return pattern;
    }

    public void setSimilarity(double similarity) {
        defaultSimilarity=similarity;
        setImageList(this.getClass());
    }
    
    
}
