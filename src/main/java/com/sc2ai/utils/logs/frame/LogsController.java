package com.sc2ai.utils.logs.frame;

import static java.awt.GraphicsDevice.WindowTranslucency.PERPIXEL_TRANSPARENT;
import static java.awt.GraphicsDevice.WindowTranslucency.TRANSLUCENT;

import java.awt.Color;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sc2ai.utils.logs.Log;

import lombok.Data;

@Data
public class LogsController{
	
	private boolean isTranslucencySupported;
	private LogsFrame logFrame;

	public LogsController(int x, int y){
        // Determine what the GraphicsDevice can support.
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        isTranslucencySupported = gd.isWindowTranslucencySupported(TRANSLUCENT);

        //If shaped windows aren't supported, exit.
        if (!gd.isWindowTranslucencySupported(PERPIXEL_TRANSPARENT)) {
            System.err.println("Shaped windows are not supported");
            System.exit(0);
        }

        //If translucent windows aren't supported, 
        //create an opaque window.
        if (!isTranslucencySupported) {
            System.out.println(
                "Translucency is not supported, creating an opaque window");
        }
        
        logFrame = new LogsFrame(x, y);

        // Set the window to 70% translucency, if supported.
        if (isTranslucencySupported) {
        	logFrame.setOpacity(0.7f);
        }

        // Display the window
        logFrame.setVisible(true);       
	}


    public void addMessage(String message) {
        logFrame.addMessage(message, null, true);
    }

    public void addEmptyMessage() {
        logFrame.addMessage("", null, true);
    }

    public void addErrorMessage(String message) {
        logFrame.addMessage(message, Color.RED, true);
    }

    public void addQueneMessage(String message){
        logFrame.addQueneMessage(message, null);
    }
    
    public void addFirstQueneMessage(String message){
        logFrame.addMessage(message, null, false);
    }

    public void removeQueneMessage(String message) {
        logFrame.removeMessage(message);
    }
    
    public void executeQueneMessage(String message){
        logFrame.setExecuteQueneMessage(message);
    }
}
