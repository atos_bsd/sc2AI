package com.sc2ai.utils.logs;

import org.springframework.stereotype.Component;

import com.sc2ai.utils.logs.frame.LogsController;

@Component
public class LogsQueueImpl extends LogsController implements Log{

    public LogsQueueImpl() {
        super(1600, 310);
    }
}
