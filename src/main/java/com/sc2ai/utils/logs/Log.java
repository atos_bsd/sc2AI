package com.sc2ai.utils.logs;

public interface Log {
    
    public void addMessage(String message);
    public void addEmptyMessage();
    public void addErrorMessage(String message);
    
    public void addQueneMessage(String message);
    public void addFirstQueneMessage(String message);
    public void executeQueneMessage(String message);
    public void removeQueneMessage(String message);
}
