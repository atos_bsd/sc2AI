package com.sc2ai.textures;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class TextyresBean {
    
    Egg egg = new Egg();
    Crystal crystal = new Crystal();
    Esc esc = new Esc();
}
