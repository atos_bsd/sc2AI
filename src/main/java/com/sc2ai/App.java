package com.sc2ai;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import com.sc2ai.utils.scheduler.JobQueue;
import com.sc2ai.objects.buildings.zerg.Hatch;


@SpringBootApplication
@EnableScheduling
@EnableAspectJAutoProxy
public class App {
	
    public static void main(String[] args){
        
        SpringApplicationBuilder springApplication = new SpringApplicationBuilder(App.class);
        springApplication.headless(false);  
        springApplication.run(args);
  	
//        System.out.println(springApplication.context().getBeanFactory().getBeanDefinitionNames());
        JobQueue jobQueue = springApplication.context().getBean(JobQueue.class);
        
        jobQueue.addTask(Hatch.class, "waiting");
//        jobQueue.addTask(ComplexActions.class, "selectDroneOnBase");
    }
}
