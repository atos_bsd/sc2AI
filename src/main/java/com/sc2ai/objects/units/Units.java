package com.sc2ai.objects.units;

import org.springframework.beans.factory.annotation.Autowired;

import com.sc2ai.config.Constants;
import com.sc2ai.objects.Objects;
import com.sc2ai.objects.buildings.zerg.Hatch;

public class Units extends Objects{
    
    @Autowired
    protected Hatch hatch;
     
    protected void buildUnit(String hotKey, Boolean waitingEgg){
        selectLarvae();
        
        do{
            if(waitingEgg)
                selectLarvae();
            s.type(hotKey);
        }
        while(!textyresBean.getEgg().isEggPresent(screen.getUnitPanelRegion()));
    }
    
    protected void buildUnit(String hotKey, Boolean waitingEgg, Integer count) {
		for(int i=0;i<count;i++)
        	buildUnit(hotKey,waitingEgg);
	}

    private void selectLarvae() {
        s.type(Constants.BASES_HOTKEY);
        s.type("s");
    }
}
