package com.sc2ai.objects;

import java.util.LinkedList;

import javax.annotation.PostConstruct;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.sc2ai.actions.Actions;
import com.sc2ai.textures.Texture;
import com.sc2ai.textures.TextyresBean;
import com.sc2ai.utils.SikuliScreen;
import com.sc2ai.utils.logs.Log;
import com.sc2ai.utils.scheduler.JobQueue;

public class Objects {

    @Autowired
    protected SikuliScreen screen;
    
    @Autowired
    @Qualifier("logsMessageImpl")
    protected Log log;
    
    @Autowired
    protected Actions actions;

    @Autowired
    protected TextyresBean textyresBean;
    
    @Autowired
    protected JobQueue jobQueue;
    
    protected Texture texture;
    protected Screen s;
    
    @PostConstruct
    public void init() {
        s = screen.getS();
        texture = new Texture(this);
    }
    
    public Pattern getImage(Integer num) {
        return (Pattern) texture.getImageList().get(num);
    }
    
    public LinkedList getImageList(){
        return texture.getImageList();
    }
}
