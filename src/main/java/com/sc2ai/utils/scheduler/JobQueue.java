package com.sc2ai.utils.scheduler;

import java.lang.reflect.Method;
import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.sc2ai.utils.logs.Log;
import com.sc2ai.utils.logs.frame.LogsController;

import lombok.Getter;
import lombok.SneakyThrows;

@Component
public class JobQueue {
    
    @Autowired
    private ApplicationContext ctx;
    
    @Autowired
    @Qualifier("logsMessageImpl")
    protected Log log;
    
    @Autowired
    private JobScheduler scheduler;
    
    @Getter
    private LinkedList<Task> taskList = new LinkedList<Task>();

    @Scheduled(fixedRate = 1000)
    void checkQueue() throws Exception {
        if(!taskList.isEmpty()){
            
            Task task = taskList.getFirst();
            
            log.addEmptyMessage();
            log.addMessage("Start execute "+task.getMethodName());
            
            Object bean = ctx.getBean(task.getCls());
                    
            executeTask(task, bean);
            
            log.addMessage("Removing task - "+task.getMethodName());
            taskList.remove(task);
        }
        
    }

    @SneakyThrows
    private void executeTask(Task task, Object bean){
        if(task.getArgsArray().length>0)
        {
            Method method = bean.getClass().getMethod(task.getMethodName(), task.getClsArray());
            method.invoke(bean, task.getArgsArray());
        }
        else{
            Method method = bean.getClass().getMethod(task.getMethodName());
            method.invoke(bean);
        }
    }
    
    public void addTask(Class<?> classType, String method, Object... args){
        taskList.addLast(new Task(classType, method, args));
    }
    
//    public void addTask(Task task) {
//        log.addMessage("Add task - "+task.getCls().getSimpleName()+"->"+task.getMethodName());
//        taskList.addLast(task);
//    }
    
    public void addDelayTask(Class<?> classType, String method, Integer delayTime, Object... args){
        log.addMessage("Add task "+classType.getSimpleName()+"->"+method+" delay "+delayTime+" s");
        scheduler.addJob(new Task(classType, method, args), delayTime, null);
    }

	public void addRepeatDelayTask(Class<?> classType, String method, int delayTime, int repeat, Object... args) {
		log.addMessage("Add task "+classType.getSimpleName()+"->"+method+" delay "+delayTime+" s");
		log.addMessage("Task repear every "+repeat+" s");
        scheduler.addJob(new Task(classType, method, args), delayTime, repeat);
	}

	public void addTaskFirst(Task task) {
		log.addMessage("Add delay task - "+task.getCls().getSimpleName()+"->"+task.getMethodName());
        taskList.addFirst(task);
	}    
    
}
