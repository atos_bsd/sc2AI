package com.sc2ai.objects.units.zerg;

import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.collections4.CollectionUtils;
import org.sikuli.script.Button;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Location;
import org.sikuli.script.Match;
import org.springframework.stereotype.Component;

import com.sc2ai.objects.units.Units;

import lombok.Data;
import lombok.SneakyThrows;

@Data
@Component
public class Drone extends Units{

    public void build(Integer count) {
        
        for(int i=0;i<count;i++)
            buildDrone();
    }
    
    private void buildDrone() {
        log.addMessage("=== Start build drone");
        buildUnit("d",true);
    }
    
    public void select(Location baseLocation){
        selectDroneOnBase(baseLocation,0);
    }

    @SneakyThrows
    private void selectDroneOnBase(Location baseLocation, Integer droneNumber){
        Match matchImage = null;
        
        actions.mouseClick(baseLocation,Button.LEFT);
        actions.selectUnitsOnScreen();
        log.addMessage("Looking for drone");
        
        matchImage = (droneNumber.equals(0))? getRandomDrone() : getDrone(droneNumber);
        
        log.addMessage("Drone finded");
        matchImage.click();
    }
    
    private Match getRandomDrone() throws FindFailed {
        Integer randomNumber;
        
        Iterator<Match> iterator =  screen.getUnitPanelRegion().findAll(getImage(0));
        //get random drone
        randomNumber = ThreadLocalRandom.current().nextInt(0, CollectionUtils.size(iterator));
        
        return getDrone(randomNumber);
    }
    
    @SneakyThrows
    private Match getDrone(Integer droneNumber) {
        Match matchImage = null;
        Iterator<Match> iterator =  screen.getUnitPanelRegion().findAll(getImage(0));
        
        for(int i=0;iterator.hasNext()&&i<=droneNumber;i++)
            matchImage = iterator.next();
        
        return matchImage;
    }
    
    public void sentDrone(Location base, Location extractorLocation) {
        
        for(int i=0;i<3;i++){
            System.out.println(extractorLocation.toString());
            selectDroneOnBase(base,i);
            actions.mouseClick(extractorLocation,Button.RIGHT);
        }
    }
}
